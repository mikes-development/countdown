<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>2023 Speaker of the House</title>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <style>
         h1, h2, h3, h4, h5, h6, p{
         text-align: center;
         }
         .center {
         display: block;
         margin-left: auto;
         margin-right: auto;
         }
      </style>
   </head>
   <body>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <h1> Current Speaker of the House Candidate Possibilities</h1>
      <a href="./voting.php">
         <h6>Current Voting for Second Speaker of the House</h6>
      </a>
      <table class="table table-striped">
         <tbody>
            <tr>
               <td>
                  <h2>Current Leader of the House</h2>
               </td>
            </tr>
            <tr>
               <td>
                  <h2>Patrick McHenry</h2>
                  <h3>Speaker of the House Pro Tempore</h3>
                  <a href="https://mchenry.house.gov">
                  <img class="center" alt="Patrick McHenry" src="./images/PatrickMcHenry-small.jpg" >
                  </a>
                  <p>North Carolina 10th District</p>
               </td>
            </tr>
         </tbody>
      </table>
      <table class="table table-striped">
         <tbody>
            <tr>
               <td colspan="3">
                  <h2>Party Nominations for Speaker of the House</h2>
               </td>
            </tr>
            <tr>
               <td>
                  <h2>Hakeem Jeffries</h2>
                  <h3>House Minority Leader/House Democratic Caucus Chair</h3>
                  <h3>Democratic Nominee for Speaker of the House</h3>
                  <p>212-0</p>
                  <a href="https://jeffries.house.gov/">
                  <img class="center" alt="" src="./images/HakeemJeffries-small.jpg" >
                  </a>
                  <p>New York 8th District</p>
               </td>
                <td>
                    <h2>Mike Johnson</h2>
                    <h3>Vice Chair Conference</h3>
                    <h3>Republican Nomionee for Speaker of the House </h3>
                    <p>0-0</p>
                    <a href="https://mikejohnson.house.gov">
                        <img src="images/MikeJohnson-small.jpg" alt="Mike Johnson" class="center" >
                    </a>
                    <p>4th Louisiana District</p>
                </td>
            </tr>
         </tbody>
      </table>
      <table class="table table-striped">
         <tbody>
            <tr>
               <td colspan="3">
                  <h2>Previous Republican Party Nominees</h2>
               </td>
            </tr>
            <tr>
               <td>
                  <h2>Tom Emmer</h2>
                  <h3>Third Nominee</h3>
                  <p>Withdrew from nomination</p>
                  <a href="https://emmer.house.gov">
                  <img src="images/TomEmmer-small.jpg" class="center" alt="Tom Emmer" >
                  </a>
                  <p>Minnesota 6th District</p>
               </td>
               <td>
                  <h2>Jim Jordan</h2>
                  <h3>Second Nominee</h3>
                  <p>Party Removed as Candidate</p>
                  <a href="https://jordan.house.gov/">
                  <img src="images/JimJordan-small.jpg" class="center" alt="Jim Jordan" >
                  </a>
                  <p>Ohio 4th District</p>
               </td>
               <td>
                  <h2>Steve Scalise</h2>
                  <h3>First Nominee</h3>
                  <p>Withdrew from nomination</p>
                  <a href="https://scalise.house.gov">
                  <img src="images/SteveScalise-small.jpg" class="center" alt="Steve Scalise" >
                  </a>
                  <p>Louisiana 1st District</p>
               </td>
            </tr>
         </tbody>
      </table>
      <table class="table table-striped">
         <tbody>
            <tr>
               <td colspan="2">
                  <h2>Current Republican Party Front Runners</h2>
               </td>
            </tr>
            <tr>
                <td>
                    <h2>Byron Donalds</h2>
                    <h3>Freedom Caucus</h3>
                    <a href="https://donalds.house.gov">
                        <img src="images/BryonDonalds-small.jpg" alt="Byron Donalds" class="center" >
                    </a>
                    <p>Florida 19th District</p>
                </td>


                <td>
                    <h2>Mike Johnson</h2>
                    <h3>Vice Chair Conference</h3>
                    <a href="https://mikejohnson.house.gov">
                        <img src="images/MikeJohnson-small.jpg" alt="Mike Johnson" class="center" >
                    </a>
                    <p>4th Louisiana District</p>
                </td>

            </tr>
         </tbody>
      </table>
      <table class="table table-striped">
         <tbody>
            <tr>
               <td colspan="5">
                  <h2>Previous Republican Party Front Runners</h2>
               </td>
            </tr>
            <tr>
               <td>
                  <h2>Steve Scalise</h2>
                  <h3> Majority Leader</h3>
                  <p>First Nominee<br />Withdrew from nomination<br /></p>
                  <a href="https://scalise.house.gov">
                  <img src="images/SteveScalise-small.jpg" class="center" alt="Steve Scalise" >
                  </a>
                  <p>Louisiana 1st District</p>
               </td>
               <td>
                  <h2>Jim Jordan</h2>
                  <h3>Chair Judiciary Committee</h3>
                  <p>Second Republican Nominee<br />Removed as Nominee</p>
                  <a href="https://jordan.house.gov/">
                  <img src="images/JimJordan-small.jpg" class="center" alt="Jim Jordan" >
                  </a>
                  <p>Ohio 4th District</p>
               </td>
               <td>
                  <h2>Kevin Hern</h2>
                  <h3>Chair Study Committee</h3>
                  <p>Withdrew before voting</p>
                  <a href="https://hern.house.gov">
                  <img src="images/KevinHern-small.jpg" class="center" alt="Kevin Hern" >
                  </a>
                  <p>Oklahoma 1st District</p>
               </td>
               <td>
                  <h2>Dan Meuser</h2>
                  <h3>Committee Member</h3>
                  <p>Withdrew 10/23/23</p>
                  <a href="https://meuser.house.gov/">
                  <img src="images/DanielMeuser-Small.jpg" alt="Daniel Meuser" class="center">
                  </a>
                  <p>9th Pennsylvania District</p>
               </td>
               <td>
                  <h2>Gary Palmer</h2>
                  <h3>Chair Party Policy Committee</h3>
                  <p>Withdrew on 10/24/23 prior to vote</p>
                  <a href="https://palmer.house.gov/">
                  <img src="images/GaryPalmer-Small.jpg" alt="Gary Palmer" class="center">
                  </a>
                  <p>6th Alabama District</p>
               </td>
            </tr>
            <tr>
               <td>
                  <h2>Pete Sessions</h2>
                  <h3>Former Chair Rules Committee</h3>
                  <p>Outed after 3rd Vote 1st Round Voting</p>
                  <a href="https://sessions.house.gov/">
                  <img src="images/PeteSessions-Small.jpg" alt="Pete Sessions" class="center">
                  </a>
                  <p>17th Texas District</p>
               </td>
               <td>
                  <h2>Jack Bergman</h2>
                  <h3>Chair Intell & Special Ops Subcommittee</h3>
                  <p>Outed after 3rd Vote 2nd Round Voting</p>
                  <a href="https://bergman.house.gov">
                  <img src="images/JackBergman-small.jpg" class="center" alt="Jack Bergman" >
                  </a>
                  <p>Michigan 1st District</p>
               </td>
               <td>
                  <h2>Austin Scott</h2>
                  <h3>Committee Member</h3>
                  <p>Outed after 3rd Vote 3rd Round Voting</p>
                  <a href="https://austinscott.house.gov">
                  <img src="images/AustinScott-small.jpg" alt="Austin Scott" class="center" >
                  </a>
                  <p>Georgia 8th District</p>
               </td>
               <td>
                  <h2>Byron Donalds</h2>
                  <h3>Freedom Caucus<br /><br /></h3>
                  <p>Outed after 3rd Vote 4th Round Voting</p>
                  <a href="https://donalds.house.gov">
                  <img src="images/BryonDonalds-small.jpg" alt="Byron Donalds" class="center" >
                  </a>
                  <p>Florida 19th District</p>
               </td>
               <td>
                  <h2>Mike Johnson</h2>
                  <h3>Vice Chair Conference</h3>
                  <p>Outed after 3rd Vote 4th Round Voting</p>
                  <a href="https://mikejohnson.house.gov">
                  <img src="images/MikeJohnson-small.jpg" alt="Mike Johnson" class="center" >
                  </a>
                  <p>Louisiana 4th District</p>
               </td>
            </tr>
            <tr>
                <td>
                    <h2>Chuck Fleischmann</h2>
                    <h3>Committee Member</h3>
                    <p>Outed after 4th Vote 1st Round Voting</p>
                    <a href="https://Fleischmann.house.gov">
                        <img src="images/CharlesFleischmann-Small.jpg" alt="Chuck Fleischmann" class="center" >
                    </a>
                    <p>3rd Tennessee District</p>
                </td>
                <td>
                    <h2>Mark Green</h2>
                    <h3>Committee Member</h3>
                    <p>Dropped out after 4th Vote 1st Round</p>
                    <a href="https://markgreen.house.gov">
                        <img src="images/MarkGreen-Small.jpg" alt="Mark Green" class="center" >
                    </a>
                    <p>7th Tennessee District</p>
                </td>
                <td>
                    <h2>Roger Williams</h2>
                    <h3>Committee Member</h3>
                    <p>Dropped out after 4th Vote 1st Round</p>
                    <a href="https://williams.house.gov">
                        <img src="images/RogerWilliams-Small.jpg" alt="Roger Williams" class="center" >
                    </a>
                    <p>25th Texas District</p>
                </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>
