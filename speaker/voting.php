<!DOCTYPE html>
<!-- Code By Webdevtrick ( https://webdevtrick.com ) -->
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>Speaker of the House Vote</title>
</head>

<body>


        <h1>Bootstrap 3 SortTable</h1>
        <table id="example" class="display" style="width:100%">
            <thead>
            <tr>
                <th scope="col">Date</th>
                <th scope="col">Time</th>
                <th scope="col">Jefries</th>
                <th scope="col">Jordan</th>
                <th scope="col">Scalise</th>
                <th scope="col">McCarthy</th>
                <th scope="col">Zeldin</th>
                <th scope="col">Donalds</th>
                <th scope="col">Garcia, Mike</th>
                <th scope="col">Emmer</th>
                <th scope="col">Boehner</th>
                <th scope="col">Granger</th>
                <th scope="col">Westerman</th>
                <th scope="col">Miller, Candice</th>
                <th scope="col">Cole</th>
                <th scope="col">Massie</th>
                <th scope="col">Present</th>
                <th scope="col">Not Voting</th>



            </tr>
            </thead>
            <tbody>
            <tr>
                <td data-table-header="Title">October 17, 2023</td>
                <td data-table-header="Authors">13:49</td>
                <td data-table-header="Journal">212</td>
                <td data-table-header="Date">200</td>
                <td data-table-header="Date">7</td>
                <td data-table-header="Date">6</td>
                <td data-table-header="Date">3</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">1</td>
            </tr>
             <tr>
                <td data-table-header="Title">October 18, 2023</td>
                <td data-table-header="Authors">13:12</td>
                <td data-table-header="Journal">212</td>
                <td data-table-header="Date">199</td>
                <td data-table-header="Date">7</td>
                <td data-table-header="Date">5</td>
                <td data-table-header="Date">3</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">1</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">0</td>
                <td data-table-header="Date">0</td>
            </tr>
          

            </tbody>
        </table>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.0/moment.min.js'></script>
<script  src="function.js"></script>

</body>
</html>
