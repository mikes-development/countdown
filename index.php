<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <style>body, h1, h2, h3, h4, h5 {
            font-family: helveticaneue, Helvetica, Arial, Utkal, sans-serif
        }

        h1, h2, h3, h4, h5 {
            font-weight: 700
        }

        :root {
            --theme-primary: #cc0000;
            --theme-background: #0c0c0c;
            --theme-divider: #404040;
            --theme-copy: #404040;
            --theme-copy-accent: #e6e6e6;
            --theme-copy-accent-hover: #ffffff;
            --theme-icon-color: #e6e6e6;
            --theme-icon-color-hover: #ffffff;
            --theme-ad-slot-background-color: #0c0c0c;
            --theme-ad-slot-text-color: #b1b1b1;
            --theme-ad-slot-text-hover: #ffffff;
            --theme-font-family: helveticaneue, Helvetica, Arial, Utkal, sans-serif;
            --theme-searchbox-border: #b1b1b1;
            --theme-copy-follow: #ffffff;
            --theme-article-spacing-top: 0px;
            --theme-link-color-hover: #6e6e6e;
            --theme-color-link: #0c0c0c;
            --theme-button-color: #6e6e6e;
            --theme-button-color-hover: #cc0000;
            --theme-edition-picker-link: #e6e6e6;
            --theme-underline-skip-ink: auto;
            --theme-paragraph__font-size: 16px;
            --theme-paragraph__line-height: 26px;
            --theme-paragraph__font-size--from-small: 16px;
            --theme-paragraph__line-height--from-small: 26px;
            --theme-paragraph__link-color: #0c0c0c;
            --theme-paragraph__link-decoration: underline;
            --theme-paragraph__link-decoration-color: var(--theme-color-link);
            --theme-paragraph__link-decoration-thickness: 1px;
            --theme-paragraph__hover-link-decoration: none;
            --theme-paragraph__hover-link-offset: 4px;
            --theme-header__hover-item-hover: var(--theme-background);
            --theme-header__item-link-color: #e6e6e6;
            --theme-header__item-link-hover-color: #ffffff;
            --theme-header__item-link-hover-background-color: transparent;
            --theme-header__mobile-dropdown-border-color: var(--theme-divider);
            --theme-header__mobile-dropdown-background: #0c0c0c;
            --theme-header__item-link-line-height: 40px;
            --theme-header__dropdown-background: #0c0c0c;
            --theme-header__dropdown-border-color: var(--theme-divider);
            --theme-header__login-button: #ffffff;
            --theme-headline__font-size: 24px;
            --theme-headline__line-height: 30px;
            --theme-headline__text-color: #0c0c0c;
            --theme-headline-sponsorship__lateral-margin: 0;
            --theme-headline__font-weight: 700;
            --theme-headline__margin-bottom: 16px;
            --theme-headline__font-family: helveticaneue, Helvetica, Arial, Utkal, sans-serif;
            --theme-headline__padding-bottom: 48px;
            --theme-headline__padding-bottom-viewport-large: 64px;
            --theme-headline__teaser-font-size: 16px;
            --theme-headline__teaser-line-height: normal;
            --theme-headline__teaser-margin-top: 0;
            --theme-headline__teaser-margin-botton: 0;
            --theme-section-headline__font-size: 36px;
            --theme-section-headline__line-height: 42px;
            --theme-section-headline__text-color: #0c0c0c;
            --theme-section-headline__font-weight: 700;
            --theme-section-headline__font-family: helveticaneue, Helvetica, Arial, Utkal, sans-serif;
            --theme-section-headline__margin-bottom: 0;
            --theme-section-headline-text__margin-top: 16px;
            --theme-section-headline-text__margin-bottom: 18px;
            --theme-section-headline-teaser__font-size: inherit;
            --theme-section-headline-teaser__color: inherit;
            --theme-subheader-h2__font-size: 24px;
            --theme-subheader-h3__font-size: 24px;
            --theme-subheader-h4__font-size: 24px;
            --theme-subheader-h5__font-size: 20px;
            --theme-subheader-h6__font-size: 16px;
            --theme-subheader-h2__line-height: 30px;
            --theme-subheader-h3__line-height: 30px;
            --theme-subheader-h4__line-height: 30px;
            --theme-subheader-h5__line-height: 26px;
            --theme-subheader-h6__line-height: 22px;
            --theme-subheader__font-family: helveticaneue, Helvetica, Arial, Utkal, sans-serif;
            --theme-subheader__font-weight: 700;
            --theme-iframe__display: block;
            --theme-list__link-decoration: underline;
            --theme-container__font-family: helveticaneue, Helvetica, Arial, Utkal, sans-serif;
            --theme-container__font-weight: 400;
            --theme-container-color--hover: #0c0c0c;
            --theme-container-image-color--hover: rgba(12, 12, 12, 0.4);
            --theme-container-text-decoration--hover: underline;
            --theme-container-text-decoration-color--hover: var(--theme-color-link);
            --theme-container-image-opacity--hover: 0.5;
            --theme-container-margin-bottom-default: 24px;
            --theme-container-margin-bottom-600: 48px;
            --theme-container-title__border-color: #e6e6e6;
            --theme-container-title__border-decorator-color: #cc0000;
            --theme-container-title__border-decorator-initial-width: 16px;
            --theme-container-title__margin-bottom: 0;
            --theme-container-title__margin-bottom-grid-4: 0;
            --theme-container-title__text-size: 12px;
            --theme-container-title__arrow-color--initial: #ffffff;
            --theme-container-title__arrow-color--hover: var(--theme-color-link);
            --theme-container-title__arrow-size: 16px;
            --theme-container-title__arrow-top-pos: 0;
            --theme-container-link__background-color: inherit;
            --theme-container-item__margin-bottom-feature-list: 32px;
            --theme-container__margin-bottom-grid-3: 24px;
            --theme-container__margin-bottom-feature-grid-3: 24px;
            --theme-container-lead-title__font-family: helveticaneue, Helvetica, Arial, Utkal, sans-serif;
            --theme-container-lead-title__font-weight: 700;
            --theme-container-lead-title__font-size: 20px;
            --theme-container-lead-title__line-height: 24px;
            --theme-container-lead-title-mobile__font-size: 16px;
            --theme-header-mobile-nav-border-color: transparent;
            --theme-text-banner__gradient-1: #cdb6f1;
            --theme-text-banner__gradient-2: #e5dbf8;
            --theme-zone__padding-bottom-default: 64px;
            --theme-zone__padding-bottom-small: 64px;
            --theme-zone__margin-bottom-default: 48px;
            --theme-zone__margin-top: -32px;
            --theme-zone-title__font-family: helveticaneue, Helvetica, Arial, Utkal, sans-serif;
            --theme-zone-title__font-size: 30px;
            --theme-zone-title__font-weight: 700;
            --theme-zone-title__line-height: 30px;
            --theme-zone-title__link-decoration: none;
            --theme-zone-title__hover-link-decoration: underline;
            --social-sharing-display: block;
            --social-sharing-margin-top: 16px;
            --social-sharing-open-close-fill: #4d4d4d;
            --social-sharing-facebook-fill: #0c0c0c;
            --social-sharing-twitter-fill: #0c0c0c;
            --social-sharing-email-fill: #0c0c0c;
            --social-sharing-link-fill: #0c0c0c;
            --theme-disclaimer-background: #e6e6e6;
            --theme-disclaimer-color: #4d4d4d;
            --theme-disclaimer-style: normal;
            --theme-disclaimer-link-color: #6a29d5;
            --theme-disclaimer-link-weight: 400;
            --theme-disclaimer-fontsize-sm: 14px;
            --theme-disclaimer-fontsize-xl: 16px;
            --theme-disclaimer-lineheight-sm: 23px;
            --theme-disclaimer-lineheight-xl: 26px;
            --theme-newsletter-form-disable-button: #c0c0c0;
            --theme-paragraph-fontsize-sm: 14px;
            --theme-paragraph-fontsize-xl: 16px;
            --theme-paragraph-lineheight-sm: 23px;
            --theme-paragraph-lineheight-xl: 26px;
            --theme-main-wrapper-rail-width: 360px;
            --theme-main-wrapper-right-rail-width: 300px;
            --theme-main-wrapper-column-gap-medium-width: 40px;
            --theme-main-wrapper-column-gap-large-width: 50px;
            --theme-primary-logo-fill: #cc0000;
            --theme-secondary-logo-fill: white;
            --theme-subheader-anchor-display: inline;
            --theme-primary-layout-color: #fafafa;
            --theme-secondary-layout-color: #fff;
            --theme-video-playlist-status-label-color: rgba(12, 12, 12, 0.7);
            --theme-video-playlist-item-hover-color: #0c0c0c
        }

        @media (min-width: 480px) {
            :root {
                --theme-section-headline-text__margin-bottom: 20px;
                --theme-container__margin-bottom-grid-3: 32px;
                --theme-container__margin-bottom-feature-grid-3: 0
            }
        }

        @media (min-width: 960px) {
            :root {
                --theme-headline__font-size: 42px;
                --theme-headline__line-height: 48px;
                --theme-section-headline__font-size: 42px;
                --theme-section-headline__line-height: 48px;
                --theme-section-headline__margin-bottom: 16px;
                --theme-subheader-h2__font-size: 36px;
                --theme-subheader-h3__font-size: 30px;
                --theme-subheader-h2__line-height: 42px;
                --theme-subheader-h3__line-height: 36px;
                --theme-container-margin-bottom-600: 0;
                --theme-container__margin-bottom-feature-grid-3: 0
            }
        }

        @media (min-width: 1280px) {
            :root {
                --theme-section-headline-text__margin-bottom: 22px
            }
        }

        @media (max-width: 959px) {
            :root {
                --social-sharing-display: none
            }
        }</style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>Countdown Clock</title>
    <style> .interactive-counter {
            width: 100%;
            max-width: 1200px;
            border: 1px solid #e6e6e6;
            border-radius: 5px;
            padding: 20px 0px;
            font-size: 1rem;
            font-weight: bold;
            margin: 0 auto;
        }

        .countdown-link {
            text-decoration: none;
            color: #262626;
        }

        .interactive-counter .clock-title {
            display: block;
            font-family: 'CNN Condensed', CNN, Helvetica, sans-serif;
            text-align: center;
            font-weight: 700;
            font-size: 1em;
            margin-top: 0;
            color: #262626;
        }

        p.text {
            text-align: center;
        }

        #cnnix-clock {
            font-weight: bold;
            text-align: center;
            font-family: 'CNN Condensed', CNN, Helvetica, sans-serif;
            font-size: 1.2em;
            width: 100%;
            padding: 0;
            margin: 0;
            list-style: none;
            justify-content: center;
            position: relative;
            display: flex;
        }

        #cnnix-clock > li {
            min-width: 77px;
            display: flex;
            flex-direction: column;
            position: relative;
            padding: 0 10px;
        }

        #cnnix-clock li:before {
            font-size: 40px;
            font-style: normal;
            font-weight: 700;
            line-height: 1;
            letter-spacing: 0;
            text-align: center;
            content: ":";
            color: #cc0000;
            position: absolute;
            left: -9px;
            top: 18px;
        }

        #cnnix-clock li:first-child::before {
            content: "";
        }

        .clock-num {
            font-weight: bold;
            font-size: 2em;
        }

        .clock-label {
            font-size: .65em;
            font-weight: normal;
        }

        @media screen and (min-width: 640px) {
            .interactive-counter .clock-title {
                font-size: 1.125em;
                margin-bottom: 5px;
            }

            .interactive-counter .clock-title:after {
                margin: 20px auto;
            }

            #cnnix-clock {
                font-size: 1.7em;
            }
        }
    </style>
</head>
<body class="layout layout-homepage cnn" data-page-type="section">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
        crossorigin="anonymous"></script>
<?php
$governmentspending1 = "0";
$governmentspending2 = "0";
$debtfight = "0";
$speaker = "1";
$governmentshutdown2024 = "1";
$portstrike2025 = "1";

if ($governmentshutdown2024 == "1") {
    echo '
   
   
            <div class="html-embed"  data-editable="settings" data-article-gutter="true">
         
         <div class="interactive-counter">
            <p class="clock-title" id="cnnix-clock-title2024">Possible Government Shutdown</p>
            <ol id="cnnix-clock">
               <li><span class="clock-num" id="days2024">—</span> <span class="clock-label" id="daysLabel2024">Days</span></li>
               <li><span class="clock-num" id="hours2024">—</span> <span class="clock-label" id="hoursLabel2024">Hours</span></li>
               <li><span class="clock-num" id="minutes2024">—</span> <span class="clock-label" id="minutesLabel2024">Minutes</span></li>
               <li><span class="clock-num" id="seconds2024">—</span> <span class="clock-label" id="secondsLabel2024">Seconds</span></li>
            </ol>
         </div>
		
      </div>
	  <br /><br />';
}

if ($portstrike2025 == "1") {
    echo '
   
   
            <div class="html-embed"  data-editable="settings" data-article-gutter="true">
            <a class="countdown-link" href="#" style="text-decoration:none;" data-toggle="modal" data-target="#exampleModal">
            

         <div class="interactive-counter">
            <p class="clock-title" id="cnnix-clock-title20241">Possible ILA Port Strike</p>
            <ol id="cnnix-clock">
               <li><span class="clock-num" id="days20241">—</span> <span class="clock-label" id="daysLabel20241">Days</span></li>
               <li><span class="clock-num" id="hours20241">—</span> <span class="clock-label" id="hoursLabel20241">Hours</span></li>
               <li><span class="clock-num" id="minutes20241">—</span> <span class="clock-label" id="minutesLabel20241">Minutes</span></li>
               <li><span class="clock-num" id="seconds20241">—</span> <span class="clock-label" id="secondsLabel20241">Seconds</span></li>
            </ol>
<p class="text">A second strike of the East and Gulf Ports is possible starting January 15, 2025.</p>
            <p class="text">The first strike of the East and Gulf Ports since the ILA striked in 1977 for nearly 9 months happened in October 2024.On October 3, 2024 at 6:34 pm Eastern, a tentative agreement has been reached for a 62% increase over 6 years and a 10% raise in the first year. Rank and File members of the ILA stil must ratify the agreement. The countdown will continue until we hear it has been ratified. The ILA has promised workers will return on October 4, 2024</p>
         </div>
		</a>
      </div>
	  <br /><br />
	  
	  
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Details on  ILA Strike</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <a target="_blank" rel="noopener noreferrer" href="https://https://www.facebook.com/ILAUnion">ILA Facebook</a>
                <hr />
                <h2>Updates</h2>
                <p>A second strike is possible starting January 15, 2025 if an agreement can not be reached.</p>
                <br  />
                <p>The ILA strike was suspended on October 3, 2024 around 6:30 p.m. Eastern until January 15. A tentative agreement has been reached between USMX and the ILA. Such agreement still remains to be voted on by rank and file members of the ILA. The agreement on wages would increase the wages by 62% over 6 years and 10% in the first year.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>';
}

//if ($governmentspending1 == "1") {
//    echo '
//              <div class="html-embed"  data-editable="settings" data-article-gutter="true">
//                <a class="countdown-link" href="shutdown/index.php" style="text-decoration:none;">
//                 <div class="interactive-counter">
//                    <p class="clock-title" id="cnnix-clock-title">Partial Government Shutdown 1 in: </p>
//                    <ol id="cnnix-clock">
//                       <li><span class="clock-num" id="days">—</span> <span class="clock-label" id="daysLabel">Days</span></li>
//                       <li><span class="clock-num" id="hours">—</span> <span class="clock-label" id="hoursLabel">Hours</span></li>
//                       <li><span class="clock-num" id="minutes">—</span> <span class="clock-label" id="minutesLabel">Minutes</span></li>
//                       <li><span class="clock-num" id="seconds">—</span> <span class="clock-label" id="secondsLabel">Seconds</span></li>
//                    </ol>
//                    <p class="text">This would include: Agrilcuture, Rural Development, FDA; Energy  Water Development; Military Construction and the VA; Transportation, Housing and Urban Development
//                 </div>
//                 </a>
//              </div>
//              <br /><br />';
//}
//
//if ($governmentspending2 == "1") {
//    echo '
//              <div class="html-embed"  data-editable="settings" data-article-gutter="true">
//                <a class="countdown-link" href="shutdown/index.php" style="text-decoration:none;">
//                 <div class="interactive-counter">
//                    <p class="clock-title" id="cnnix-clock-title5">Partial Government Shutdown 2 in: </p>
//                    <ol id="cnnix-clock">
//                       <li><span class="clock-num" id="days5">—</span> <span class="clock-label" id="daysLabel">Days</span></li>
//                       <li><span class="clock-num" id="hours5">—</span> <span class="clock-label" id="hoursLabel">Hours</span></li>
//                       <li><span class="clock-num" id="minutes5">—</span> <span class="clock-label" id="minutesLabel">Minutes</span></li>
//                       <li><span class="clock-num" id="seconds5">—</span> <span class="clock-label" id="secondsLabel">Seconds</span></li>
//                    </ol>
//                    <p class="text">This would include: Defense; Commerce, Justice & Science; Financial Services & General Government; Homeland Security; Interior, Enviroment and Related Agencies; Labor, Health and Human Services and Education; Legisltative Branch
//                 </div>
//                 </a>
//              </div>
//              <br /><br />';
//}

// if ($debtfight == "1") {
//     echo '

//             <div class="html-embed"  data-editable="settings" data-article-gutter="true">

//          <div class="interactive-counter">
//             <p class="clock-title" id="cnnix-clock-title1">US Debt Ceiling Crisis</p>
//             <ol id="cnnix-clock">
//                <li><span class="clock-num" id="days1">—</span> <span class="clock-label" id="daysLabel1">Days</span></li>
//                <li><span class="clock-num" id="hours1">—</span> <span class="clock-label" id="hoursLabel1">Hours</span></li>
//                <li><span class="clock-num" id="minutes1">—</span> <span class="clock-label" id="minutesLabel1">Minutes</span></li>
//                <li><span class="clock-num" id="seconds1">—</span> <span class="clock-label" id="secondsLabel1">Seconds</span></li>
//             </ol>
//          </div>

//       </div>
// 	  <br /><br />';
// }

if ($speaker == "1") {
    echo '
       <div class="html-embed"  data-editable="settings" data-article-gutter="true">
          <a class="countdown-link" href="./speaker/index.php" style="text-decoration:none;">
         <div class="interactive-counter">
            <p class="clock-title" id="cnnix-clock-title2">No Speaker of the House 2023</p>
            <ol id="cnnix-clock">
               <li><span class="clock-num" id="days2">21</span> <span class="clock-label" id="daysLabel2">Days</span></li>
               <li><span class="clock-num" id="hours2">21</span> <span class="clock-label" id="hoursLabel2">Hours</span></li>
               <li><span class="clock-num" id="minutes2">8</span> <span class="clock-label" id="minutesLabel2">Minutes</span></li>
               <li><span class="clock-num" id="seconds2">—</span> <span class="clock-label" id="secondsLabel2">Seconds</span></li>
            </ol>
            <p class="text">First time in History where the House removed a Speaker of the House. This paralyzed the legistive branch of the U.S. Government for nearly 3 weeks as the House could not do any business</p>
         </div>
		 </a>
      </div>';
}
?>
<script>


    // function upTime() {
    //     //used by first government shutdown
    //     now = new Date();
    //     countTo = Date.UTC(2024, 0, 20, 5);
    //     //countTo = Date.UTC(2023, 9, 1, 4);
    //     //This shows the following: October 1, 2023 0400 UTC or October 1, 2023 0000 local time
    //     // 2023
    //     // monthindex = 9 which is October
    //     // day of month = 1
    //     // hour of day = 0400
    //     //monthIndex, day, hour, minute, second, millisecond
    //     //2023 = year, 0-99 goes from 1900 to 1999, other values are actual year
    //     //9 = month index 0-11, defaults to 0
    //     //1 = day of the month, defaults to 1
    //     //4 = 0 - 23 for hour of the day defaults to 0
    //     //minutes defaults to 0
    //     //seconds, defeaults to 0
    //     //milliseconds, defaults to 0
    //     difference = (countTo - now);
    //     if (difference > 0) {
    //         days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
    //         hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
    //         mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
    //         secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
    //     } else if (difference <= 0) {
    //         difference = -difference;
    //         document.getElementById('cnnix-clock-title').firstChild.nodeValue = "The US government has been shut down for";
    //         days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
    //         hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
    //         mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
    //         secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
    //     }
    //     var dayLabel = (days > 1 || days == 0) ? " days" : " day";
    //     var hourLabel = (hours > 1 || hours == 0) ? " hours" : " hour";
    //     var minLabel = (mins > 1 || mins == 0) ? " minutes" : " minute";
    //     var secLabel = (secs > 1 || secs == 0) ? " seconds" : " second";
    //     document.getElementById('days').firstChild.nodeValue = days;
    //     document.getElementById('hours').firstChild.nodeValue = hours;
    //     document.getElementById('minutes').firstChild.nodeValue = mins;
    //     document.getElementById('seconds').firstChild.nodeValue = secs;
    // }
    //
    // upTime();
    // setInterval(upTime, 1000);


    function portstrike2025() {
        const now = new Date();
        const countTo = Date.UTC(2025, 0,15 , 4);
        //countTo = Date.UTC(2023, 9, 1, 4);
        // 2023
        // monthindex = 9 which is October
        // day of month = 1
        // hour of day = 0400
        //monthIndex, day, hour, minute, second, millisecond
        //2023 = year, 0-99 goes from 1900 to 1999, other values are actual year
        //9 = month index 0-11, defaults to 0
        //1 = day of the month, defaults to 1
        //4 = 0 - 23 for hour of the day defaults to 0
        //minutes defaults to 0
        //seconds, defeaults to 0
        //milliseconds, defaults to 0
        difference = (countTo - now);
        let days;
        let hours;
        let mins;
        let secs;
        if (difference > 0) {
            days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
            hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
            mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
            secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
        } else if (difference <= 0) {
            difference = -difference;
            document.getElementById('cnnix-clock-title20241').firstChild.nodeValue = "ILA Port Strike started: ";
            days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
            hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
            mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
            secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
        }
        var dayLabel = (days > 1 || days == 0) ? " days" : " day";
        var hourLabel = (hours > 1 || hours == 0) ? " hours" : " hour";
        var minLabel = (mins > 1 || mins == 0) ? " minutes" : " minute";
        var secLabel = (secs > 1 || secs == 0) ? " seconds" : " second";
        document.getElementById('days20241').firstChild.nodeValue = days;
        document.getElementById('hours20241').firstChild.nodeValue = hours;
        document.getElementById('minutes20241').firstChild.nodeValue = mins;
        document.getElementById('seconds20241').firstChild.nodeValue = secs;
    }

    portstrike2025();
    setInterval(portstrike2025, 1000);

    function govtfunding2025() {
        const now = new Date();
        const countTo = Date.UTC(2025, 2, 14, 4);
        //countTo = Date.UTC(2023, 9, 1, 4);
        //This shows the following: October 1, 2023 0400 UTC or October 1, 2023 0000 local time
        // 2023
        // monthindex = 9 which is October
        // day of month = 1
        // hour of day = 0400
        //monthIndex, day, hour, minute, second, millisecond
        //2023 = year, 0-99 goes from 1900 to 1999, other values are actual year
        //9 = month index 0-11, defaults to 0
        //1 = day of the month, defaults to 1
        //4 = 0 - 23 for hour of the day defaults to 0
        //minutes defaults to 0
        //seconds, defeaults to 0
        //milliseconds, defaults to 0
        difference = (countTo - now);
        let days;
        let hours;
        let mins;
        let secs;
        if (difference > 0) {
            days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
            hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
            mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
            secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
        } else if (difference <= 0) {
            difference = -difference;
            document.getElementById('cnnix-clock-title2024').firstChild.nodeValue = "The US government has been shutdown:";
            days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
            hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
            mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
            secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
        }
        var dayLabel = (days > 1 || days == 0) ? " days" : " day";
        var hourLabel = (hours > 1 || hours == 0) ? " hours" : " hour";
        var minLabel = (mins > 1 || mins == 0) ? " minutes" : " minute";
        var secLabel = (secs > 1 || secs == 0) ? " seconds" : " second";
        document.getElementById('days2024').firstChild.nodeValue = days;
        document.getElementById('hours2024').firstChild.nodeValue = hours;
        document.getElementById('minutes2024').firstChild.nodeValue = mins;
        document.getElementById('seconds2024').firstChild.nodeValue = secs;
    }

    govtfunding2025();
    setInterval(govtfunding2025, 1000);


    // function debt() {
    //     const now = new Date();
    //     const countTo = Date.UTC(2025, 0, 1, 5);
    //     //countTo = Date.UTC(2023, 9, 1, 4);
    //                     //This shows the following: October 1, 2023 0400 UTC or October 1, 2023 0000 local time
    //                             // 2023
    //                             // monthindex = 9 which is October
    //                             // day of month = 1
    //                             // hour of day = 0400
    //                     //monthIndex, day, hour, minute, second, millisecond
    //                     //2023 = year, 0-99 goes from 1900 to 1999, other values are actual year
    //                     //9 = month index 0-11, defaults to 0
    //                     //1 = day of the month, defaults to 1
    //                     //4 = 0 - 23 for hour of the day defaults to 0
    //                     //minutes defaults to 0
    //                     //seconds, defeaults to 0
    //                     //milliseconds, defaults to 0
    //                         difference = (countTo - now);
    //     let days;
    //     let hours;
    //     let mins;
    //     let secs;
    //     if (difference > 0) {
    //         days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
    //         hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
    //         mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
    //         secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
    //     } else if (difference <= 0) {
    //         difference = -difference;
    //         document.getElementById('cnnix-clock-title1').firstChild.nodeValue = "The US government breached the debt ceiling for:";
    //         days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
    //         hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
    //         mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
    //         secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
    //     }
    //     var dayLabel = (days > 1 || days == 0) ? " days" : " day";
    //     var hourLabel = (hours > 1 || hours == 0) ? " hours" : " hour";
    //     var minLabel = (mins > 1 || mins == 0) ? " minutes" : " minute";
    //     var secLabel = (secs > 1 || secs == 0) ? " seconds" : " second";
    //     document.getElementById('days1').firstChild.nodeValue = days;
    //     document.getElementById('hours1').firstChild.nodeValue = hours;
    //     document.getElementById('minutes1').firstChild.nodeValue = mins;
    //     document.getElementById('seconds1').firstChild.nodeValue = secs;
    // }
    // debt();
    // setInterval(debt, 1000);

    //   /* function speaker() {
    //     now = new Date();
    //     countTo = Date.UTC(2023, 9, 3,20, 42);
    //                     //countTo = Date.UTC(2023, 9, 1, 4);
    //                     //This shows the following: October 1, 2023 0400 UTC or October 1, 2023 0000 local time
    //                             // 2023
    //                             // monthindex = 9 which is October
    //                             // day of month = 1
    //                             // hour of day = 0400
    //                     //monthIndex, day, hour, minute, second, millisecond
    //                     //2023 = year, 0-99 goes from 1900 to 1999, other values are actual year
    //                     //9 = month index 0-11, defaults to 0
    //                     //1 = day of the month, defaults to 1
    //                     //4 = 0 - 23 for hour of the day defaults to 0
    //                     //minutes defaults to 0
    //                     //seconds, defeaults to 0
    //                     //milliseconds, defaults to 0
    //                         difference = (countTo - now);
    //     if (difference > 0) {
    //         days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
    //         hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
    //         mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
    //         secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
    //     } else if (difference <= 0) {
    //         difference = -difference;
    //         document.getElementById('cnnix-clock-title2').firstChild.nodeValue = "No Speaker of the House";
    //         days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
    //         hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
    //         mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
    //         secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
    //     }
    //     var dayLabel = (days > 1 || days == 0) ? " days" : " day";
    //     var hourLabel = (hours > 1 || hours == 0) ? " hours" : " hour";
    //     var minLabel = (mins > 1 || mins == 0) ? " minutes" : " minute";
    //     var secLabel = (secs > 1 || secs == 0) ? " seconds" : " second";
    //     document.getElementById('days2').firstChild.nodeValue = days;
    //     document.getElementById('hours2').firstChild.nodeValue = hours;
    //     document.getElementById('minutes2').firstChild.nodeValue = mins;
    //     document.getElementById('seconds2').firstChild.nodeValue = secs;
    // }
    // speaker();
    // setInterval(speaker, 1000);*/

    // function upTime1() {
    //     now = new Date();
    //     countTo = Date.UTC(2024, 1, 3,5);
    //                     //countTo = Date.UTC(2023, 9, 1, 4);
    //                     //This shows the following: October 1, 2023 0400 UTC or October 1, 2023 0000 local time
    //                             // 2023
    //                             // monthindex = 9 which is October
    //                             // day of month = 1
    //                             // hour of day = 0400
    //                     //monthIndex, day, hour, minute, second, millisecond
    //                     //2023 = year, 0-99 goes from 1900 to 1999, other values are actual year
    //                     //9 = month index 0-11, defaults to 0
    //                     //1 = day of the month, defaults to 1
    //                     //4 = 0 - 23 for hour of the day defaults to 0
    //                     //minutes defaults to 0
    //                     //seconds, defeaults to 0
    //                     //milliseconds, defaults to 0
    //                         difference = (countTo - now);
    //     if (difference > 0) {
    //         days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
    //         hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
    //         mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
    //         secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
    //     } else if (difference <= 0) {
    //         difference = -difference;
    //         document.getElementById('cnnix-clock-title').firstChild.nodeValue = "A Partial US government has been shut down for";
    //         days = Math.floor(difference / (60 * 60 * 1000 * 24) * 1);
    //         hours = Math.floor((difference % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
    //         mins = Math.floor(((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
    //         secs = Math.floor((((difference % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
    //     }
    //     var dayLabel = (days > 1 || days == 0) ? " days" : " day";
    //     var hourLabel = (hours > 1 || hours == 0) ? " hours" : " hour";
    //     var minLabel = (mins > 1 || mins == 0) ? " minutes" : " minute";
    //     var secLabel = (secs > 1 || secs == 0) ? " seconds" : " second";
    //     document.getElementById('days5').firstChild.nodeValue = days;
    //     document.getElementById('hours5').firstChild.nodeValue = hours;
    //     document.getElementById('minutes5').firstChild.nodeValue = mins;
    //     document.getElementById('seconds5').firstChild.nodeValue = secs;
    // }
    // upTime1();
    // setInterval(upTime1, 1000);
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</body>

